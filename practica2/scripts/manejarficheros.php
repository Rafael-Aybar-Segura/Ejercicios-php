<?php
    // Comprobamos la ruta de los archivos y que podemos escribir en ellos
    $rutacoef ="/srv/http/Ejercicios-php/practica2/ficheros/coeficientes.txt";
    $rutaexpo ="/srv/http/Ejercicios-php/practica2/ficheros/exponentes.txt";
    $rutaval = "/srv/http/Ejercicios-php/practica2/ficheros/valores.txt";
    if (is_file($rutacoef) && is_readable($rutacoef)&& is_file($rutaexpo) && is_readable($rutaexpo) && is_file($rutaval) && is_readable($rutaval)) {
    // abrimos los ficheros con permisos de lectura:
    $archivocoef=fopen($rutacoef, "r");
    $archivoexpo=fopen($rutaexpo, "r");
    $archivoval= fopen($rutaval, "r");
    //Guardamos los archivos en un array
    $coef = file($rutacoef);
    $expo = file($rutaexpo);
    $val= file($rutaval);
    //Guardamos los polinomios en una variable
    $polinomio1 = $coef[0]."x <sup>".$expo[0]."</sup>"." + ".$coef[1]."x <sup>".$expo[1]."</sup> + $coef[2]x <sup>$expo[2] </sup> + $coef[3]x <sup>$expo[3]</sup>";

    // procedemos a hacer lo mismo para el polinomio2
    $polinomio2 = $coef[4]."x <sup>".$expo[4]."</sup>"." + ".$coef[5]."x <sup>".$expo[5]."</sup> + $coef[6]x <sup>$expo[6] </sup> + $coef[7]x <sup>$expo[7]</sup>";
    
    //Cerramos los archivos*/
    fclose($archivocoef);
    fclose($archivoexpo);
    fclose($archivoval);
    }
    else {
        die("Ruta incorrecta, asuencia de permisos de lectura, o no es un fichero");
    }
?>
<?php
    include '/srv/http/Ejercicios-php/practica2/scripts/manejarficheros.php';
    //Convertimos a enteros todos los números del array
    for ($e=0; $e <=7 ; $e++) { 
        $coef[$e] =(float)$coef[$e];
        $expo[$e] = (float)$coef[$e];
    }
    //Suma de polinomios
    echo "<br>";
    $suma = $coef[0]+ $coef[4];
    $suma2 = $coef[1]+$coef[5];
    $suma3 = $coef[2]+$coef[6];
    $suma4 = $coef[3]+$coef[7];
    //echo "$suma x<sup>$expo[0]</sup> +$suma2 x<sup>$expo[1]</sup> + $suma3 x <sup>$expo[2]</sup> + $suma4 x<sup>$expo[3]";
    //creamos una tabla:
    echo "<h3> Resultado de la suma: </h3><br>";
    echo "<table border>";
    echo "<tr><td>X<sup>3</sup></td><td>X<sup>2</sup></td><td>X<sup>1</sup></td><td>X<sup>0</sup></td></tr>";
    echo "<tr><td>$suma</td><td>$suma2</td><td>$suma3</td><td>$suma4</td></tr>";
    echo "</table><br>";
    $polsuma = (string) "(".$suma."x<sup>3</sup>+".$suma2."x<sup>2</sup> +".$suma3."x+".$suma4.")";
    //Eliminamos los resultados que se correspondan con 0x ,dado que da igual el exponente, siempre será el mismo al multiplicarse por 0
    str_ireplace("+0x","",$polsuma);
    //Cambiamos los signos para la resta
    str_ireplace("+-","-",$polsuma);
    echo "$polsuma <br>";
        
    //Resta de polinomios (p1 +p2)
    $resta = $coef[0]- $coef[4];
    $resta2 = $coef[1]-$coef[5];
    $resta3 = $coef[2]-$coef[6];
    $resta4 = $coef[3]-$coef[7];

    echo "<h3> Resultado de la resta del primer polinomio y el segundo polinomio: </h3><br>";
    echo "<table border>";
    echo "<tr><td>X<sup>3</sup></td><td>X<sup>2</sup></td><td>X<sup>1</sup></td><td>X<sup>0</sup></td></tr>";
    echo "<tr><td>$resta</td><td>$resta2</td><td>$resta3</td><td>$resta4</td></tr>";
    echo "</table><br>";
    $polresta = (string) "(".$resta."x<sup>3</sup>+".$resta2."x<sup>2</sup> +".$resta3."x+".$resta4.")";
    //Volvemos a eliminar 0x
    $polresta=str_ireplace("+0x","",$polresta);
    //Cambiamos el signo
    $polresta = str_ireplace("+-","-",$polresta);
    echo "$polresta <br>";
    //Evaluación del polinomio:
    //Volvemos a cambiar el tipo de dato
    for ($i=0; $i <count($val) ; $i++) { 
        $val[$i]= (float) $val[$i];
    }
    echo "<br>";
    echo "<b>Sustituimos el polinomio de la suma </b>";
    echo "<table border>";
    echo "<tr><td><b>Valor del fichero</b></td><td><b>Evaluar polinomio</b></td><td><b>Resultado</b></td></tr>";
    for ($u=0; $u <count($val) ; $u++) { 
        //Evaluamos el polinomio resultante de la suma 
        $valor = pow($val[$u] * $suma,3) + pow($val[$u]*$suma2,2) + ($val[$u]*$suma3) + $suma4;
        echo "<tr><td>$val[$u]</b></td><td>$val[$u] $polsuma</td><td>$valor</td></tr>";
    }
    echo "</table>";
    
?>
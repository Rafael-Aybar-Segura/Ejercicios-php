<?php
    $precio = $_POST['precref'];
    $monedas = $_POST['monedas'];
    //Comprobamos que los campos estén vacíos y sean números
    if (isset($precio) && isset($monedas) && is_numeric($precio) && is_numeric($monedas) && $precio > 0 && $monedas > 0) {
        echo "Has introducido $monedas centimos </br>";
        if ($precio <= $monedas) {
            $vuelta = $monedas - $precio;
            echo "Tu vuelta es de $vuelta céntimos </br>";
            $cambio=array(200, 100, 50, 20, 10, 5);
            
            $devolver=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            
            for($i=0; $i<count($cambio); $i++)
            {
                if($vuelta>=$cambio[$i])
                {
                    $devolver[$i]=floor($vuelta/$cambio[$i]);
            
                    $vuelta=$vuelta-($devolver[$i]*$cambio[$i]);
                }
            }
            
            for($i=0; $i<count($cambio); $i++)
            {
                if($devolver[$i]>0)
                {
                        echo "El cambio es de: ".$devolver[$i]." monedas de ".$cambio[$i]." centimos.<br>";
                
                }
            }
                    }
                    else {
                        die("Debes introducir más dinero");
                    }
                }
                else {
                    die("Introduce datos coherentes");
                }
?>
<?php
    //Inicamos la cadena:
    $cadena = "1 2 3 ... aqui empieza la cadena de caracteres y aqui termina";
    
    //Indicamos la longitud de la cadena con la función srlen()
    echo "La cadena tiene ". strlen($cadena). " caracteres. <br>";
    
    //Para sustituir cadenas de caracteres usamos una expresión regular con la
    //función preg_replace
    $cadenanueva = preg_replace('/[a-z]/','/', $cadena);
    echo "$cadenanueva <br>";
    
    //Mostramos los caracteres
    $char1 = substr($cadena,0,1);
    $char3 = substr($cadena, 2,3);
    $ultimochar = substr($cadena, -1);
    echo "El primer carácter es $char1, el tercer carácter es $char3 y el último carácter es $ultimochar <br>";
    
    //Añadimos el carácter al final de la cadena
    $cad2 = $cadena."@";
    echo "La cadena nueva es $cad2";
?>
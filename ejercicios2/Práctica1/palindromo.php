<?php
    //Declaramos las variables, con espacio en blanco a propósito
    $palindromo = " recono cer ";
    
    //Con la función trim borramos los espacios al principio y al final de
    //la cadena pero no los espacios interiores, por lo que usamos la función
    //str_ireplace (insesible a mayúsculas o minúsculas para localizar espacios)
    //en blanco en toda la cadena.
    $palstr = str_ireplace(" ","",$palindromo);

    //Comprobamos que es un palíndrimo poniendo la cadena del revés con la función
    //strrev
    if ($palstr === strrev($palstr)) {
        echo "$palstr es un palíndormo";
    }
    else {
        echo "No es un palíndromo";
    }
?>
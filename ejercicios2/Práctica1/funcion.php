<?php
    //Comprobamos que nos llega la variable del formulario y es un numero
    if (isset($_POST['num']) && is_numeric($_POST['num'])) {
        $num = (int) $_POST['num'];
        //Comprobamos que el número está en el rango deseado
        if ($num <= 12 && $num >= 1) {
            //Declaramos la función con el parámetro de entrada
            function decirmes($num){
                if ($num === 1) {
                    echo "Es Enero";
                }
                elseif ($num === 2) {
                    echo "Es Febrero";
                }
                elseif ($num === 3) {
                    echo "Es Marzo";
                }
                elseif ($num === 4) {
                    echo "Es Abril";
                }
                elseif ($num === 5) {
                    echo "Es Mayo";
                }
                elseif ($num === 6) {
                    echo "Es Junio";
                }
                elseif ($num === 7) {
                    echo "Es Julio";
                }
                elseif ($num === 8) {
                    echo "Es Agosto";
                }
                elseif ($num === 9) {
                    echo "Es Septiembre";
                }
                elseif ($num === 10) {
                    echo "Es Octubre";
                }
                elseif ($num === 11) {
                    echo "Es Noviembre";
                }
                else {
                    echo "Es Diciembre";
                }
            }
            echo decirmes($num);
        }
        else {
            echo "El número no está en el rango permitido";
        }
    }
    else {
        echo "Debes introducir el número";
    }
?>
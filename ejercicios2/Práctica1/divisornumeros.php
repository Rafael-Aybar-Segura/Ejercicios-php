<?php
    if (isset($_POST['num1']) && isset($_POST['num2'])) {
        if (is_numeric($_POST['num1']) && is_numeric($_POST['num2'])) {
            $num1 = $_POST['num1'];
            $num2 = $_POST['num2'];
            if ($num1 % $num2 == 0) {
                echo "$num1 y $num2 son divisores";
            }
            elseif ($num2 % $num1 == 0) {
                echo "$num2 y $num1 son divisores";
            }
            else {
                echo "Los números introducidos no son divisores";
            }
        }
        else {
            echo "El tipo de dato no es válido";
        }
    }
    else {
        echo "Introduce los datos";
    }
?>